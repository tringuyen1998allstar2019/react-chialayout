import React from 'react';
import Header from './header';
import Carousel from './carousel';
import ListPhone from './list-phone';
import ListLaptop from './list-laptop';
import Promotion from './promotion';

export default function Bt() {
    return (
    <div className="bg-dark">
    <Header/>
    <Carousel/>
    <ListPhone/>
    <ListLaptop/>
    <Promotion/>
    </div>
)

}